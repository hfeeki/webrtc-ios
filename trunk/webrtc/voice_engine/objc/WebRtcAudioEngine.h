//
//  WebRtcAudioEngine.h
//  voice_engine
//
//  Created by chenzs on 14-1-24.
//
//

#import <Foundation/Foundation.h>

@interface WebRtcAudioEngine : NSObject

//是否已经初始化
@property  BOOL isInitinazed;

//create and delete function
-(BOOL)voE_Create;
-(BOOL)voE_Delete;

//initalization and termination
-(NSInteger)voE_Init;
-(NSInteger)voE_Terminate;

//channel function
-(NSInteger)voE_CreateChannel;
-(NSInteger)voe_DeleteChannel:(NSInteger)channel;

//Receiver and Destination function
-(NSInteger)voE_SetLocalReceiver:(NSInteger)channel localPort:(NSInteger)port;
-(NSInteger)voE_SetSendDestination:(NSInteger)chanel destPort:(NSInteger)port destIp:(NSString*)ipaddr;

//media functions
-(NSInteger)voE_StartListen:(NSInteger)channel;
-(NSInteger)voE_StartPlayout:(NSInteger)channel;
-(NSInteger)voE_StartSend:(NSInteger)channel;
-(NSInteger)voE_StopListen:(NSInteger)channel;
-(NSInteger)voE_StopPlayout:(NSInteger)channel;
-(NSInteger)voE_StopSend:(NSInteger)channel;

//volum and hardware
-(NSInteger)voE_SetSpeakerVolume:(NSInteger)volume;
-(NSInteger)voE_SetLoudSpeakerStatus:(BOOL)enable;

//codec-setting functions
-(NSInteger)voE_CountOfCodecs;
-(NSArray *)voE_GetCodecs;
-(NSInteger)voE_SetSendCodec:(NSInteger)channel codecIndex:(NSInteger)index;

//voiceEngine function
-(NSInteger)voE_SetECStatus:(BOOL)enable;
-(NSInteger)voE_SetAGCStauts:(BOOL)enable;
-(NSInteger)voE_SetNSSStauts:(BOOL)enable;

@end
